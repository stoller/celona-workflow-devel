#!/usr/bin/perl -w
#
# Copyright (c) 2000-2023 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use English;
use strict;
use Getopt::Std;
use Data::Dumper;
use POSIX;

sub usage()
{
    print "Usage: setup-iperf.pl [-R]\n";
    exit(1);
}
my $optlist   = "R";
my $TMPDIR    = "/var/tmp";
my $IPERFIP   = "192.168.2.128";

my %options = ();
if (! getopts($optlist, \%options)) {
    die("usage");
}

#
# Install from here.
#
if (! -e "/etc/.iperf-installed") {
    if (! -e "/local/repository/install-iperf.sh") {
	die("/local/repository/install-iperf.sh not found");
    }
    system("/local/repository/install-iperf.sh");
    if ($?) {
	die("Could not install iperf");
    }
}

my @allbuses = (4410, 6186, 6185, 6183, 6182, 6181, 6180, 5175, 4964,
		4817, 4734, 4604, 4603, 4555, 4410, 4409, 4407, 6362,
		6363, 6364);

# Command line override
my @buses = (@ARGV ? @ARGV : @allbuses);
    
#
# iperf servers. One for every possible bus. Silly, I know. 
#
foreach my $bus (@buses) {
    if (! grep {$_ eq $bus} @allbuses) {
	die("No such bus $bus in the bus list\n");
    }
    my $port = 50000 + $bus;
    my $pidfile = "/var/tmp/iperf-${bus}.pid";

    if (defined($options{"R"})) {
	if (-e $pidfile) {
	    my $pid = `/bin/cat $pidfile`;
	    chomp($pid);
	    system("sudo kill $pid");
	    sleep(1);
	}
    }
    print "Starting iperf server for bus $bus on port $port\n";
    my $command =
	"daemon -N -r -n iperf-${bus} --delay=15 ".
	"   --output=/var/tmp/iperf-${bus}.log ".
	"   --pidfile=$pidfile -- ".
	" /usr/local/bin/iperf3 -s -p $port -B $IPERFIP --forceflush ".
	"   --rcv-timeout 30000 --snd-timeout 30000 ";
    #print "$command\n";
    system($command);
    if ($?) {
	die("Could not start iperf server for bus $bus on port $port");
    }
    sleep(1);
}
