"""Celona OTA workflow experiment.
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import IG extensions
import geni.rspec.igext as ig
# Emulab specific extensions.
import geni.rspec.emulab as emulab
# Spectrum specific extensions.
import geni.rspec.emulab.spectrum as spectrum
# Route specific extensions.
import geni.rspec.emulab.route as route

IMAGEURN   = "urn:publicid:IDN+emulab.net+image+PowderTeam:cots-jammy-image"
SHVLANNAME = "CEGCAP"
UESTART    = "/local/repository/start-ue.pl -s "
IPERFSTART = "/local/repository/start-iperf.pl"

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Influx server.
pc.defineParameter(
    "influxHost","InfluxDB host name and optional port (host[:port])",
    portal.ParameterType.STRING,"")
pc.defineParameter(
    "influxPassword","InfluxDB admin password",
    portal.ParameterType.STRING,"")

# Retrieve the values the user specifies during instantiation.
params = pc.bindParameters()

# We must get the two parameters for this to work. 
#if params.influxHost.strip() == "":
#    pc.reportError(portal.ParameterError("You must specify the influx host",
#                                         ["influxHost"]))
#    pass
#if params.influxPassword.strip() == "":
#    pc.reportError(portal.ParameterError("You must specify the influx password",
#                                         ["influxPassword"]))
#    pass

# Throw the errors back to the user.
#pc.verifyParameters()

# Update the UE command line, those scripts need the influx parameters
UESTART = UESTART + params.influxHost + " " + params.influxPassword

# Request all routes
busroute = request.requestBusRoute("allroutes")
busroute.disk_image = IMAGEURN
busroute.addService(pg.Execute(shell="bash", command=UESTART))

#
# Since we cannot start iperf servers on the edge, we use a another node
# that is connected to the same shared vlan as the proxy. This is a wired
# ethernet, so the additional latency is small.
#
# We run a set of iper3 servers on this node, one for every bus.
#
con = request.RawPC("iperfhost")
con.component_manager_id = 'urn:publicid:IDN+emulab.net+authority+cm'
con.hardware_type = "d710"
con.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
con.addService(pg.Execute(shell="bash", command=IPERFSTART))
con.startVNC()

# Hook up to the shared vlan with the proxy
lan1 = request.LAN("lan1")
lan1.vlan_tagging = False
lan1.setNoBandwidthShaping()
lan1.connectSharedVlan(SHVLANNAME)

iface = con.addInterface()
iface.addAddress(pg.IPv4Address("192.168.2.128", "255.255.255.0"))
lan1.addInterface(iface)

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
