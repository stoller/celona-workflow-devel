#!/usr/bin/perl -w
#
# Copyright (c) 2000-2023 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
use English;
use strict;
use Getopt::Std;
use Data::Dumper;
use POSIX;

# Drag in path stuff so we can find emulab stuff.
BEGIN { require "/etc/emulab/paths.pm"; import emulabpaths; }

sub usage()
{
    print "Usage: setup-ue.pl [-s] <influx host:port> <influx password>\n";
    print "Usage: setup-ue.pl -S\n";
    exit(1);
}
my $optlist   = "sS";
my $start     = 0;
my $QCM       = "/usr/local/bin/quectel-CM -s celonadns -4";
my $TMPDIR    = "/var/tmp";
my $TTYDEV    = "/dev/ttyUSB2";
my $CHAT      = "chat -t 1 -sv '' AT OK";
my $IMSIINFO  = 'AT+CIMI';
my $IMSICMD   = "$CHAT '${IMSIINFO}' OK < $TTYDEV > $TTYDEV";

# Need to figure these out. 
my $INFLUXDB_HOSTNAME;
my $INFLUXDB_PORT;
my $INFLUXDB_PASSWORD;
my $GPSPIPE_HOSTNAME = "control";

# Protos
sub RunChat($);
sub GetIMSI();
sub StartUE();

my %options = ();
if (! getopts($optlist, \%options)) {
    die("usage");
}
if (defined($options{"s"})) {
    $start++;
}
if (defined($options{"S"})) {
    StartUE();
    exit(0);
}
usage()
    if (@ARGV != 2);

if ($ARGV[0] =~ /^([^:]+):(.*)$/) {
    $INFLUXDB_HOSTNAME = $1;
    $INFLUXDB_PORT = $2;
}
else {
    die("Bad host:port argument\n");
}
$INFLUXDB_PASSWORD = $ARGV[1];

# Influx tag
my $HOST = `hostname`;
chomp($HOST);
if ($HOST =~ /^([^\.]+)\.(.*)$/) {
    $HOST = $1;
}

if (! -e "/etc/.ue-installed") {
    if (! -e "/local/repository/install-ue.sh") {
	die("/local/repository/install-ue.sh not found");
    }
    system("/local/repository/install-ue.sh");
    if ($?) {
	die("Could not install UE support");
    }
}

# Also an influx tag
my $IMSI = GetIMSI();
if (!defined($IMSI)) {
    die("Could not get the UEs IMSI");
}

#
# Create the env variables file that the scripts use.
#
if (open(VARS, "> $TMPDIR/ue.env")) {
    print VARS "export INFLUXDB_HOSTNAME=\"${INFLUXDB_HOSTNAME}\"\n";
    print VARS "export INFLUXDB_PORT=\"${INFLUXDB_PORT}\"\n";
    print VARS "export INFLUXDB_USERNAME=\"metrics\"\n";
    print VARS "export INFLUXDB_PASSWORD=\"${INFLUXDB_PASSWORD}\"\n";
    print VARS "export INFLUXDB_BUCKET=\"metrics\"\n";
    print VARS "export INFLUXDB_ORG=\"metrics\"\n";
    print VARS "export INFLUXDB_DATABASE=\"metrics\"\n";
    print VARS "export INFLUXDB_GLOBAL_TAGS=\"host=${HOST},imsi=${IMSI}\"\n";
    print VARS "export INFLUXDB_SSL=\"True\"\n";
    print VARS "export GPSPIPE_HOSTNAME=\"${GPSPIPE_HOSTNAME}\"\n";

    #
    # And add them to our environment.
    #
    $ENV{"INFLUXDB_HOSTNAME"}    = ${INFLUXDB_HOSTNAME};
    $ENV{"INFLUXDB_PORT"}        = ${INFLUXDB_PORT};
    $ENV{"INFLUXDB_USERNAME"}    = "metrics";
    $ENV{"INFLUXDB_PASSWORD"}    = ${INFLUXDB_PASSWORD};
    $ENV{"INFLUXDB_BUCKET"}      = "metrics";
    $ENV{"INFLUXDB_ORG"}         = "metrics";
    $ENV{"INFLUXDB_DATABASE"}    = "metrics";
    $ENV{"INFLUXDB_GLOBAL_TAGS"} = "host=${HOST},imsi=${IMSI}";
    $ENV{"INFLUXDB_SSL"}         = "True";
    $ENV{"GPSPIPE_HOSTNAME"}     = ${GPSPIPE_HOSTNAME};
}
else {
    die("Could not open $TMPDIR/ue.env for writing: $!");
}

#
# Install from here.
#
if (! -e "/etc/.ue-installed") {
    system("/local/repository/install-ue.sh");
    if ($?) {
	die("Could not install UE support");
    }
}

if ($start) {
    StartUE();
}
exit(0);

sub StartUE()
{
    system("sudo daemon -N -r -n qcm -o /var/tmp/q.log -- $QCM");
    if ($?) {
	die("Could not start quectel-CM");
    }
    sleep(1);
    system("sudo /local/repository/attach-detach.pl");
    if ($?) {
	die("Could not attach");
    }
    sleep(1);
    system("sudo /local/repository/uemetrics-to-influx.py -b");
    if ($?) {
	die("Could not start uemetrics-to-influx.py");
    }
    sleep(1);
    system("/local/repository/gpspipe-to-influx.py -b");
    if ($?) {
	die("Could not start gpspipe-to-influx.py");
    }
    sleep(1);
    system("/local/repository/network-metrics.pl -b");
    if ($?) {
	die("Could not start network-metrics.py");
    }
    sleep(1);
    system("/local/repository/iperf-metrics.pl -b");
    if ($?) {
	die("Could not start iperf-metrics.py");
    }
}

sub RunChat($)
{
    my ($chatcmd)  = @_;
    my $command = "sudo /bin/sh -c \"$chatcmd\"";
    my $output  = "";
    
    #
    # This open implicitly forks a child, which goes on to execute the
    # command. The parent is going to sit in this loop and capture the
    # output of the child. We do this so that we have better control
    # over the descriptors.
    #
    my $pid = open(PIPE, "-|");
    if (!defined($pid)) {
	die("popen");
    }
    
    if ($pid) {
	while (<PIPE>) {
	    $output .= $_;
	}
	close(PIPE);
	if ($?) {
	    print STDERR $output;
	    return undef;
	}
    }
    else {
	open(STDERR, ">&STDOUT");
	exec($command);
    }
    return $output;
}

sub GetIMSI()
{
    my $output = RunChat("$IMSICMD");
    if (!defined($output)) {
	return undef;
    }
    my @lines = split(/\n/, $output);
    while (@lines) {
	my $line = shift(@lines);
	if ($line =~ /^(\d+)/) {
	    return $1;
	}
    }
    return undef;
}
