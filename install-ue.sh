#!/bin/sh
set -x

SCRIPTNAME=$0
TMPDIR="/var/tmp"

#
# Might not be on the local cluster, so need to use the urn to
# see who the actual creator is.
#
GENIUSER=`geni-get user_urn | awk -F+ '{print $4}'`
if [ $? -ne 0 ]; then
    echo "ERROR: could not run geni-get user_urn!"
    exit 1
fi
if [ $USER != $GENIUSER ]; then
    sudo -u $GENIUSER $SCRIPTNAME
    exit $?
fi

sudo apt update
sudo apt-get -y install --no-install-recommends \
     libtimedate-perl libjson-perl gpsd-clients pip daemon iperf3 libfile-tail-perl
if [ $? -ne 0 ]; then
    echo 'ERROR: apt-install utilities failed'
    exit 1
fi

sudo pip install influxdb-client pyserial pytz
if [ $? -ne 0 ]; then
    echo 'ERROR: pip install install failed'
    exit 1
fi

# So we get the routes setup when we attach.
sudo cp /local/repository/udhcpc.script /etc/udhcpc/default.script
sudo chmod +x /etc/udhcpc/default.script

# marker
sudo touch /etc/.ue-installed

