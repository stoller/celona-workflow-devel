import traceback
import os
from datetime import date, datetime, timedelta
from zoneinfo import ZoneInfo
from typing import List, NoReturn

from dagster import op, graph, job, fs_io_manager, Out, Output
from dagster import execute_job, reconstructable
from dagster import Failure, failure_hook, HookContext, get_dagster_logger

from .params import input_params
from .resources import resource_defs
from . import workflowCreds

#
# start() drags stuff out of the incoming params to pass along.
#
@op(config_schema={"params" : dict},
    out={"params": Out()})
def Initialize(context) -> dict:
    params = context.op_config["params"]
    return params

#
# Start the AP experiment if not running.
#
@op(required_resource_keys={"portal"},
    out={"start": Out(is_required=False, dagster_type=bool),
         "started": Out(is_required=False, dagster_type=bool)})   
def CheckAPsAllocated(context, params: dict):
    #context.log.info(str(status))
    #params = context.op_config["params"]
    portal = context.resources.portal
    #context.log.info(str(portal))

    result = portal.run(action="experiment.status",
                        name=params["ap_experiment"],
                        proj=params["proj"],
                        max_wait_time=5, errors_are_fatal=False,
                        wait_for_status=False,
                        context=context)

    # Return False if the experiment is allready running
    if result["result_code"] != 0:
        yield Output(True, "start")
    else:
        yield Output(False, "started")
        pass
    pass

@op(required_resource_keys={"portal"})
def AllocateAPs(context, params: dict, start: bool) -> bool:
    #params = context.op_config["params"]
    portal = context.resources.portal

    #
    # Start the experiment and wait. 
    #
    result = portal.run(action="experiment.create",
                        name=params["ap_experiment"],
                        proj=params["proj"],
                        profile=params["ap_profile_name"],
                        paramset=params["ap_paramset"],
                        sshpubkey=workflowCreds.portalCreds["sshpubkey"],
                        max_wait_time=3600,
                        # Polling Interval, does not belong here.
                        interval=30,
                        errors_are_fatal=True,
                        wait_for_status=True,
                        wait_for_execute_status=True,
                        context=context)
    context.log.debug(str(result))

    if result["result_code"] != 0:
        #
        # Either a start error or a status error. If the later then the experiment
        # still exists and probably needs to be terminated. Need to think about that.
        #
        if "status" in result:
            description = result["status"].get("failure_message","unknown")
            pass
        else:
            description = result["error"]
            pass
        raise Failure(description=description)
    
    return True

@op
def APsAllocated(context, started: bool) -> bool:
    return True

@op
def APsReady(context, started: list) -> bool:
    assert started == [True] or started == [False]
    return started == [True] or started == [False]

#
# See if the experiment needs to be started
#
@op(required_resource_keys={"portal"})
def maybeStartBusExperiment(context, params: dict, Ready: bool) -> bool:
    #context.log.info(str(status))
    #params = context.op_config["params"]
    portal = context.resources.portal
    #context.log.info(str(portal))

    result = portal.run(action="experiment.status",
                        name=params["bus_experiment"],
                        proj=params["proj"],
                        max_wait_time=5, errors_are_fatal=False,
                        wait_for_status=False,
                        context=context)
    #context.log.info(str(result))

    # For now, throw a clear error if the experiment exists.
    if result["result_code"] == 0:
        description = "Experiment already exists"
        if "status" in result and "status" in result["status"]:
            status = result["status"].get("result","unknown")
            if status == "canceled":
                description += ", but is in the canceled state. "
                description += "Please change the 'bus_experiment' parameter "
                description += "in the Launchpad."
                pass
        raise Failure(description=description)

    #
    # Start the experiment and wait. 
    #
    # Bus experiments have to terminate the same night started, at 11pm.
    # But not more then 15 hours in case not reserved. Need to think about this.
    #
    elevenPM = datetime.fromisoformat(str(date.today()) + " 23:00:00")
    elevenPM = elevenPM.replace(tzinfo=ZoneInfo("US/Mountain"))
    plus15   = datetime.now(tz=ZoneInfo("US/Mountain")) + timedelta(hours=15)
    
    if plus15 < elevenPM:
        stop = plus15
        pass
    else:
        stop = elevenPM
        pass
    stop = stop.strftime("%Y-%m-%d %H:%M:%S")
    
    result = portal.run(action="experiment.create",
                        name=params["bus_experiment"],
                        proj=params["proj"],
                        profile=params["bus_profile_name"],
                        paramset=params["bus_paramset"],                        
                        bindings=params["bus_bindings"],
                        stop=stop,
                        sshpubkey=workflowCreds.portalCreds["sshpubkey"],
                        max_wait_time=3600,
                        # Polling Interval, does not belong here.
                        interval=30,
                        errors_are_fatal=False,
                        wait_for_status=True,
                        wait_for_execute_status=False,
                        context=context)
    context.log.debug(str(result))

    if result["result_code"] != 0:
        #
        # Either a start error or a status error. If the later then the experiment
        # still exists and probably needs to be terminated. Need to think about that.
        #
        if "status" in result:
            description = result["status"].get("failure_message","unknown")
            pass
        else:
            description = result["error"]
            pass
        raise Failure(description=description)

    return True

@job(
    config = {
        "ops" : {
            "Initialize" : {
                "config" : {
                    # The defaults can be overridden in the WEB UI.
                    "params" : input_params
                }
            }
        }
    },
    resource_defs = resource_defs
)
def startBuses():
    params = Initialize()
    start, started = CheckAPsAllocated(params)
    r1 = AllocateAPs(params, start)
    r2 = APsAllocated(started)
    # Merge, for the graph to look nice.
    merged = APsReady([r1, r2])
    buses = maybeStartBusExperiment(params, merged)
    pass

