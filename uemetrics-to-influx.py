#!/usr/bin/python

import getopt
import os
import sys
import threading
import logging
import json
import subprocess
from influxdb_client import InfluxDBClient
from influxdb_client.client.write.point import Point
from influxdb_client.client.write_api import SYNCHRONOUS
import datetime
import dateutil.parser
import traceback
import signal
import time
import serial

TTYDEV    = "/dev/ttyUSB2"
CELLINFO  = 'AT+QENG=\"servingcell\"'

# Run in daemon mode when not debugging.
daemon = 0;

LOG = logging.getLogger(__name__)

def usage():
    print("Usage: " + sys.argv[0] + " -b")
    pass

class InfluxDBEnvConfig(object):

    def __init__(self,name):
        self.enabled = True
        self.name = name
        self.hostname = os.getenv("INFLUXDB_HOSTNAME","localhost")
        self.port = int(os.getenv("INFLUXDB_PORT",8086))
        self.username = os.getenv("INFLUXDB_USERNAME",None)
        self.password = os.getenv("INFLUXDB_PASSWORD",None)
        self.database = os.getenv("INFLUXDB_DATABASE","telegraf")
        self.token = os.getenv("INFLUXDB_TOKEN",None)
        self.bucket = os.getenv("INFLUXDB_BUCKET",None)
        self.org = os.getenv("INFLUXDB_ORG",None)
        self.ssl = os.getenv("INFLUXDB_SSL",False)
        if self.ssl in [1,"true","True","1"]:
            self.ssl = True
            self.url = "https://" + self.hostname + ":" + str(self.port)
        else:
            self.url = "http://" + self.hostname + ":" + str(self.port)
        self.measurement_prefix = os.getenv("INFLUXDB_MEASUREMENT_PREFIX","")
        self.global_tags = os.getenv("INFLUXDB_GLOBAL_TAGS",{})
        if self.global_tags:
            td = dict()
            for kvp in self.global_tags.split(","):
                (k,v) = kvp.split("=",1)
                td[k] = v
            self.global_tags = td
            print(str(self.global_tags))

class InfluxDBLogger(threading.Thread):
    def __init__(self,outputs=[]):
        super(InfluxDBLogger,self).__init__()
        self.clients = dict()
        self.configs = dict()
        self.queues = dict()
        self.lock = threading.Lock()
        self.cv = threading.Condition(lock=self.lock)
        self._cancel = False

        for output in outputs:
            if not output.enabled:
                continue
            if output.name in self.clients:
                raise ConfigError("duplicate influxdb output name %s" % (output.name,))
            client = InfluxDBClient(
                output.url, timeout=2000, org=output.org,
                username=output.username,password=output.password,
                token=output.token)
            self.clients[output.name] = client.write_api(write_options=SYNCHRONOUS)
            self.configs[output.name] = output
            self.queues[output.name] = []

    def log(self,points=[]):
        if not points:
            return
        if not self.is_alive():
            self.start()
        for name in self.clients:
            newpoints = []
            for p in points:
                print(str(p))
                np = dict()
                if self.configs[name].measurement_prefix:
                    ms = "%s%s" % (self.configs[name].measurement_prefix,
                                   p["measurement"])
                else:
                    ms = p["measurement"]
                np = dict(measurement=ms)
                if "time" in p:
                    np["time"] = p["time"]
                if "tags" not in p:
                    np["tags"] = dict()
                else:
                    np["tags"] = p["tags"]
                for (t,v) in self.configs[name].global_tags.items():
                    p["tags"][t] = v
                if "fields" not in p:
                    np["fields"] = dict()
                else:
                    np["fields"] = p["fields"]
                newpoints.append(Point.from_dict(np))
            self.lock.acquire()
            self.queues[name].extend(newpoints)
            self.lock.release()
        with self.cv:
            self.cv.notify()

    def run(self):
        tel = []
        try:
            import urllib3.exceptions
            tel.append(urllib3.exceptions.ReadTimeoutError)
        except:
            pass
        try:
            import requests.exceptions
            tel.append(requests.exceptions.ReadTimeout)
        except:
            pass
        if not tel:
            tel = [Exception]
        tel = tuple(tel)
        while True:
            if self._cancel:
                LOG.debug("stopping influxdb event log thread")
                return
            tosend = dict()
            sent = dict()
            remaining = 0
            self.cv.acquire()
            for name in self.queues.keys():
                if len(self.queues[name]):
                    tosend[name] = min(len(self.queues[name]),32)
                    if len(self.queues[name]) > 32:
                        remaining += len(self.queues[name]) - 32
            self.cv.release()
            if tosend:
                for name in tosend.keys():
                    try:
                        config = self.configs[name]
                        self.clients[name].write(
                            config.bucket, config.org, 
                            self.queues[name][0:tosend[name]])
                        sent[name] = tosend[name]
                        LOG.debug("sent %d points to output %s",tosend[name],name)
                    except tel:
                        LOG.warn("timeout while sending %d points to output %s",
                                  tosend[name],name)
                        remaining += tosend[name]
                    except Exception:
                        LOG.error("failed to send %d points to output %s",
                                  tosend[name],name,exc_info=sys.exc_info())
                        remaining += tosend[name]
            if sent:
                self.cv.acquire()
                for name in sent.keys():
                    del self.queues[name][0:sent[name]]
                self.cv.release()
            timeout = None
            if remaining:
                timeout = 8
            with self.cv:
                self.cv.wait(timeout=timeout)

    def close(self):
        LOG.debug("closing influxdb event log")
        self._cancel = True
        with self.cv:
            self.cv.notify()

#
# +QENG: "servingcell",<state>,"NR5G-SA",<duplex_mode>,<MCC>,<MNC>,<cellID>,<PCID>,<TAC>,<ARFCN>,<band>,<NR_DL_bandwidth>,<RSRP>,<RSRQ>,<SINR>,<scs>,<srxlev>
#

class MetricsReader(object):
    def __init__(self,logger):
        self.logger = logger
        self.nometrics = {"RSRP" : None, "RSRQ" : None, "SINR" : None,
                          "CELLID" : None, "PCID" : None}

    def _getMetrics(self):
        response = ''
        with serial.Serial('/dev/ttyUSB2', 115200, timeout=0.1) as ser:
            command = CELLINFO + '\r'
            ser.write(command.encode())
            while True:
                chunk = ser.read(512)
                if not chunk:
                    # Timeout occurred
                    break
                response += chunk.decode()
                pass
            pass
        
        for line in response.splitlines():
            if line.find("NOCONN") > 0:
                tokens = line.split(",")
                if len(tokens) >= 13:
                    CELLID = tokens[6]
                    PCID   = int(tokens[7])
                    RSRP   = int(tokens[12])
                    RSRQ   = int(tokens[13])
                    SINR   = int(tokens[14])
                    return {"RSRP" : RSRP, "RSRQ" : RSRQ, "SINR" : SINR,
                            "CELLID" : CELLID, "PCID" : PCID}
                pass
            pass
        return self.nometrics

    def run(self):
        while True:
            time.sleep(0.5)
            try:
                metrics = self._getMetrics()
                if not metrics:
                    continue
            except Exception:
                LOG.warn("Failed to get metrics from device",
                         exc_info=sys.exc_info())
                time.sleep(1)
                continue
            LOG.info(str(metrics))
            point = {}
            point["tags"] = {}
            point["time"] = datetime.datetime.utcnow()
            point["fields"] = metrics
            point["measurement"] = "uemetrics"
            self.logger.log([point])
            pass
        pass

def sigh(signo,frame):
    ilog.close()
    exit(0)

if __name__ == "__main__":
    #
    # Process program arguments.
    # 
    try:
        opts, req_args =  getopt.getopt(sys.argv[1:], "hb", ["help", "daemon"])
        for opt, val in opts:
            if opt in ("-h", "--help"):
                usage()
                sys.exit()
                pass
            elif opt in ("-b", "--daemon"):
                daemon = 1;
                pass
            pass
        pass
    except getopt.error as e:
        print(e.args[0])
        usage()
        sys.exit(2)
        pass

    signal.signal(signal.SIGINT,sigh)
    signal.signal(signal.SIGHUP,sigh)
    signal.signal(signal.SIGTERM,sigh)

    try:
        import urllib3
        urllib3.disable_warnings()
    except:
        pass

    ic = InfluxDBEnvConfig("overwatch")

    if daemon:
        try:
            fp = open("/var/tmp/uemetrics-to-influx.log", "a");
            sys.stdout = fp
            sys.stderr = fp
            sys.stdin.close();
            pass
        except:
            print("Could not open log file for append")
            sys.exit(1);
            pass
        
        pid = os.fork()
        if pid:
            sys.exit(0)
            pass
        os.setsid();
        pass

    logging.basicConfig()
    if os.getenv("UEMETRICS_TO_INFLUX_DEBUG","0") == "1":
        LOG.setLevel(logging.DEBUG)

    ilog = InfluxDBLogger(outputs=[ic])
    metrics = MetricsReader(ilog)

    try:
        ilog.start()
    except:
        traceback.print_exc()
        exit(1)
    while True:
        try:
            metrics.run()
        except:
            traceback.print_exc()
            ilog.close()
            exit(1)
    ilog.close()
    exit(1)
